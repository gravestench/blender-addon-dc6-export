# ------------------------------------------------------------------------
#    PNG -> DC6 conversion
# ------------------------------------------------------------------------

import bpy
import os
from operator import itemgetter

from math import sqrt

from PIL import Image

from . import dc6

DC6Encoder = dc6.DC6FileEncoder


def start(scene, start, end, src_dir, dst_dir, dst_name, dst_palette):
    """Encodes rendered png files into DC6

    start, end - indices of the start/end frames
    src_dir - the source directory (where rendered png's are located)
    dst_dir - where encoded dc6 files will be placed
    dst_name - the filename, without the extension, for the encoded dc6 file
    dst_palette - The key of the palette which will be used (eg. 'UNITS')
    """
    directions_count = scene.DC6Plugin.rig_directions
    count = end - start + 1

    # ensure dc6 extension in destination filename
    if dst_name[-4:] != '.dc6':
        dst_name += '.dc6'

    folder = bpy.path.abspath(src_dir)
    # for singledirection, filenames are like `0001.png`
    # for single frame, filenames like `_0.png`
    # for multi direction+frames, filename like `0001_0.png`
    # we just need to get them and sort the array
    ordered_filepaths = []
    for filename in os.listdir(folder):
        filepath = os.path.join(folder, filename)
        if os.path.isfile(filepath) or os.path.islink(filepath):
            if '.png' in filepath:
                ordered_filepaths.append(filepath)
    ordered_filepaths.sort()

    print('Generating DC6 from:\n', '\n'.join(ordered_filepaths))
    framedatas = []
    for path in ordered_filepaths:
        # remap to target palette
        img = Image.open(path).convert('P')
        img = remap_image_to_target_palette(img, dst_palette)
        w = img.width
        h = img.height
        framedatas.append([b for b in img.getdata()])

    dc6file = DC6Encoder(directions_count, count, w, h, framedatas)

    dc6_path = os.path.join(bpy.path.abspath(dst_dir), dst_name)

    print('GENERATING DC6:', dc6_path)
    dc6file.fileinfo()

    outfile = open(dc6_path, "wb")
    outfile.write(dc6file.getbytes())
    outfile.close()


def load_image(filepath):
    """Loads an image as PNG using PIL.Image and returns the object"""
    img = Image.open(filepath).convert('P')
    return img


def remap_image_to_target_palette(src_img, dst_palette):
    """Remaps the palette of the source image to the destination palette
    NOTE: This preserves the ordering of the destination palette so that
    colormaps will work correctly.
    """
    src_palette = src_img.getpalette()
    len_src = len(src_palette)
    len_dst = len(dst_palette)

    # group like [ [r,g,b], ... ]
    with src_palette as s:
        src_palette_grp = [s[i*3:(i+1)*3] for i in range((len_src+2)//3)]

    with dst_palette as d:
        # dst palette is BGR order so we reverse it with [::-1]
        dst_palette_grp = [d[i*3:(i+1)*3][::-1] for i in range((len_dst+2)//3)]

    # make a mapping of closest colors of src to dst palette
    mapping = []
    for src_color in src_palette_grp:
        r = src_color[0]
        g = src_color[1]
        b = src_color[2]
        closest = get_closest_color_index(r, g, b, dst_palette_grp)
        mapping.append(closest)

    src_data = src_img.getdata()
    dst_data = [mapping[idx] for idx in src_data]

    src_img.putdata(dst_data)
    src_img.putpalette([b for b in dst_palette])
    return src_img


def file_bytes(filepath):
    """Get bytes for entire file"""
    return open(filepath, "rb").read()


def get_closest_color_index(r, g, b, p):
    """Given r,g,b and an array of RGB tuples p, find the tuple that is
    closest to r,g,b and return the index
    """
    distances = [sqrt((r-c[0])**2 + (g-c[1])**2 + (b-c[2])**2) for c in p]
    closest = min(enumerate(distances), key=itemgetter(1))[0]
    return closest
